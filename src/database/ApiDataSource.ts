import { DataSource } from 'typeorm'
import path from 'path'
import config from '../config'

// In tests run migrations out of the box
const isTestEnv = process.env.NODE_ENV === 'test'

const dbSource = new DataSource({
    type: 'postgres',
    username: config.db.username,
    host: config.db.host,
    database: 'movie-api-db',
    password: config.db.password,
    entities: [path.join(__dirname, './../**/*.model.*')],
    migrations: [path.join(__dirname, './database/migrations/*')],
    migrationsRun: true,
    logging: true,
    synchronize: isTestEnv,
    port: config.db.port,
    logger: 'simple-console',
    extra: {
        poolSize: 20,
        connectionTimeoutMillis: 2000,
        query_timeout: 1000,
        statement_timeout: 1000
    }
})

export default dbSource
