import { auth } from 'express-oauth2-jwt-bearer'

const authenticationMiddleware = auth({
    audience: process.env.AUTH_AUDIENCE,
    issuerBaseURL: process.env.AUTH_TENANT_URL,
    tokenSigningAlg: 'RS256'
})

export default authenticationMiddleware
