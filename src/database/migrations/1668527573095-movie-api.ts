import { MigrationInterface, QueryRunner } from 'typeorm'

export class movieApi1668527573095 implements MigrationInterface {
    name = 'movieApi1668527573095'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "movie_review" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "author_name" character varying NOT NULL, "content" character varying NOT NULL, "rating" integer NOT NULL, "movieId" uuid, CONSTRAINT "PK_1fda640af19993fc39acf43efd5" PRIMARY KEY ("id"))`
        )
        await queryRunner.query(
            `CREATE TABLE "movie_model" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "overview" character varying, "tagline" character varying, "runtime" TIME, "release_date" TIMESTAMP NOT NULL, "revenue" integer, "poster_path" character varying, CONSTRAINT "PK_67679acf42c8b2c9341d0a3d37c" PRIMARY KEY ("id"))`
        )
        await queryRunner.query(
            `ALTER TABLE "movie_review" ADD CONSTRAINT "FK_79a05494c92b4c0f78e3794d8e1" FOREIGN KEY ("movieId") REFERENCES "movie_model"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "movie_review" DROP CONSTRAINT "FK_79a05494c92b4c0f78e3794d8e1"`
        )
        await queryRunner.query(`DROP TABLE "movie_model"`)
        await queryRunner.query(`DROP TABLE "movie_review"`)
    }
}
