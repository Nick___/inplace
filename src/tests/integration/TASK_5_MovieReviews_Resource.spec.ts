import { faker } from '@faker-js/faker'
import { Server } from 'http'
import request from 'supertest'
import { StartedTestContainer } from 'testcontainers'
import { v4 as uuid } from 'uuid'

import createServer from '../../createServer'
import { ApplicationServer } from '../../types'
import createFakeMovie from '../seed/createFakeMovie'
import getAuthTokenTest from '../utils/getAuthTokenTest'
import createTestDbContainer from './createTestDbContainer'

let app: Server,
    dbContainer: StartedTestContainer,
    appServer: ApplicationServer,
    testMovieId: string,
    authToken: string

beforeAll(async () => {
    // starts a Docker container with a postgres instance insie
    dbContainer = await createTestDbContainer()

    const serverConfig = {
        db: {
            port: dbContainer.getMappedPort(5432),
            host: dbContainer.getHost(),
            username: 'root',
            password: 'theSeniorDev'
        }
    }

    appServer = await createServer(serverConfig)
    app = appServer.app

    const testMovie = await createFakeMovie()
    testMovieId = testMovie.id

    authToken = await getAuthTokenTest()
})

afterAll(async () => {
    await appServer?.dbClient?.destroy() // disconnecting the Database Client
    await dbContainer.stop() // Stoping the Database Container
})

describe('[TASK 5] Add tests for MovieReviews and implement the endpoint for it: TDD', () => {
    describe('When we make a GET request to the /api/movie/:id/reviews endpoint', () => {
        test('Then the list of reviews for the specific movie is returned', async () => {
            // ARRANGE
            // 1. Create a Movie
            const newMovie = await createFakeMovie()

            // 2. Setup the Review Mock Data
            const testReviewData = {
                author_name: faker.name.fullName(),
                content: faker.lorem.paragraph(10),
                rating: Number(faker.random.numeric(1))
            }

            // 3. Create the Review
            const createReviewResponse = await request(app)
                .post(`/api/movies/${newMovie.id}/reviews`)
                .send(testReviewData)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(200)

            // ACT
            // 4. Fetch the review
            const getReviewResponse = await request(app)
                .get(`/api/movies/${newMovie.id}/reviews`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(200)

            // ASSERT
            expect(getReviewResponse.body.length).toEqual(1)
            expect(getReviewResponse.body[0]).toMatchObject(testReviewData)
        })
    })
    describe('When we make a POST request to the /api/movie/:id/reviews endpoint', () => {
        test('When the movie exists and the data is valid the movie review is created', async () => {
            // ARRANGE
            // 1. Create a Movie
            const newMovie = await createFakeMovie()

            // 2. Setup the Review Mock Data
            const reviewData = {
                content:
                    'I really enjoyed the movie, particuallry the ending. This is by far one of the best movie out there.',
                author_name: 'Foo Bar',
                rating: 8
            }

            // 3. ACT: Create the Review
            const createReviewResponse = await request(app)
                .post(`/api/movies/${newMovie.id}/reviews`)
                .send(reviewData)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(200)

            // 4. GET the review
            const getReviewResponse = await request(app)
                .get(
                    `/api/movies/${newMovie.id}/reviews/${createReviewResponse.body.id}`
                )
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(200)

            expect(getReviewResponse.body).toMatchObject(reviewData)
        })

        test('When the movie does not exist a 404 is returned', async () => {
            const movieId = uuid()
            const response = await request(app)
                .post(`/api/movies/${movieId}/reviews`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(404)
        })

        test('When the movie id is not correct a 400 Bad request is returned', async () => {
            const movieId = 'fake-id'
            const response = await request(app)
                .post(`/api/movies/${movieId}/reviews`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(400)
        })

        describe('When the review data is not correct a 400 Bad request is returned', () => {
            test('When the review content is not long enough', async () => {
                // 1. Create a Movie
                const newMovie = await createFakeMovie()
                const reviewData = {
                    content: 'Too short ..',
                    author_name: 'Foo Bar',
                    rating: 8
                }
                const response = await request(app)
                    .post(`/api/movies/${newMovie.id}/reviews`)
                    .send(reviewData)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(400)
            })
            test('When the review author is missing', async () => {
                // 1. Create a Movie
                const newMovie = await createFakeMovie()
                const reviewData = {
                    content: 'This was one of the best movie I have watched',
                    rating: 8
                }
                const response = await request(app)
                    .post(`/api/movies/${newMovie.id}/reviews`)
                    .send(reviewData)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(400)
            })
            test('When the review rating is missing', async () => {
                const newMovie = await createFakeMovie()

                const reviewData = {
                    content: 'This was one of the best movie I have watched',
                    author_name: 'Foo Bar'
                }
                const response = await request(app)
                    .post(`/api/movies/${newMovie.id}/reviews`)
                    .send(reviewData)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(400)
            })
            test('When the review rating is greater then 10 ', async () => {
                const newMovie = await createFakeMovie()

                const reviewData = {
                    content: 'This was one of the best movie I have watched',
                    title: 'Great Movie',
                    rating: 400
                }
                const response = await request(app)
                    .post(`/api/movies/${newMovie}/reviews`)
                    .auth(authToken, { type: 'bearer' })
                    .send(reviewData)
                    .set('Content-Type', 'application/json')
                    .expect(400)
            })
        })
    })

    describe('When we make a DELETE request to the /api/movie/:id/reviews endpoint', () => {
        test('When the movie does not exist a 404 is returned', async () => {
            const movieId = uuid()
            await request(app)
                .delete(`/api/movies/${movieId}/reviews`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(404)
        })

        test('When the review does not exist a 404 is returned', async () => {
            const reviewId = uuid()
            const newMovie = await createFakeMovie()

            await request(app)
                .delete(`/api/movies/${newMovie.id}/reviews/${reviewId}`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(404)
        })

        test('When the review id is invalid a 400 code is returned', async () => {
            const reviewId = 'fake-id'
            const newMovie = await createFakeMovie()

            await request(app)
                .delete(`/api/movies/${newMovie.id}/reviews/${reviewId}`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(400)
        })

        test('If the movie id is valid the movie review is deleted', async () => {
            const newMovie = await createFakeMovie()

            const reviewData = {
                content:
                    'I really enjoyed the movie, particulary the ending. This is by far one of the best movie out there.',
                author_name: 'Foo Bar',
                rating: 8
            }

            const response = await request(app)
                .post(`/api/movies/${newMovie.id}/reviews`)
                .send(reviewData)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(200)

            await request(app)
                .delete(
                    `/api/movies/${newMovie.id}/reviews/${response.body.id}`
                )
                .auth(authToken, { type: 'bearer' })
                .expect(204)

            await request(app)
                .get(`/api/movies/${newMovie.id}/reviews/${response.body.id}`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(404)
        })
    })
    describe('When we make a PATCH request to the /api/movie/:id/reviews/:review_id endpoint', () => {
        test('When the update data is correct', async () => {
            const newMovie = await createFakeMovie()

            const reviewData = {
                content:
                    'I really enjoyed the movie, particulary the ending. This is by far one of the best movie out there.',
                author_name: 'Foo Bar',
                rating: 8
            }
            // 1. create a review
            const response = await request(app)
                .post(`/api/movies/${newMovie.id}/reviews`)
                .send(reviewData)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(200)

            const updateReviewData = {
                rating: 6
            }

            const responsePatch = await request(app)
                .patch(`/api/movies/${newMovie.id}/reviews/${response.body.id}`)
                .send(updateReviewData)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(200)

            expect(responsePatch.body).toMatchObject({
                ...reviewData,
                ...updateReviewData
            })
        })
        test('When the movie does not exist a 404 is returned', async () => {
            const movieId = uuid()
            await request(app)
                .patch(`/api/movies/${movieId}/reviews`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(404)
        })

        test('When the review does not exist a 404 is returned', async () => {
            const reviewId = uuid()
            await request(app)
                .patch(
                    `/api/movies/f74cf1ca-8c7b-435b-96c6-e4448a653596/reviews/${reviewId}`
                )
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(404)
        })

        test('When the review id is invalid a 400 code is returned', async () => {
            const reviewId = 'fake-id'
            const response = await request(app)
                .patch(
                    `/api/movies/f74cf1ca-8c7b-435b-96c6-e4448a653596/reviews/${reviewId}`
                )
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
                .expect(400)
        })
    })
})
