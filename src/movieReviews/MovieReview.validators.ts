import Joi from 'joi'

export const MovieReviewCreateInputSchema = Joi.object({
    author_name: Joi.string().min(3).max(500).required(),
    content: Joi.string().min(20).max(10000).allow(''),
    rating: Joi.number().min(0).max(10).required(),
    movie_id: Joi.string().uuid().optional()
})

export const MovieReviewUpdateInputSchema = Joi.object({
    author_name: Joi.string().min(3).max(500),
    content: Joi.string().min(20).max(10000).allow(''),
    rating: Joi.number().min(0).max(10).optional()
})
