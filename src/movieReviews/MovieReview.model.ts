import {
    BaseEntity,
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne
} from 'typeorm'
import Movie from '../movies/Movie.model'

@Entity()
export default class MovieReview extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column()
    author_name!: string

    @Column()
    content!: string

    @Column()
    rating!: number

    @ManyToOne(() => Movie, (movie) => movie.reviews)
    movie!: Movie
}
