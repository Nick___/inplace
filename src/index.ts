import dotnev from 'dotenv-flow'

// loading env variables before other imports
dotnev.config()

import createServer from './createServer'
import config from './config'
import { ApplicationServer } from './types'

createServer(config).then((appSever: ApplicationServer) => {
    appSever.app.listen(config.port, () => {
        console.info(`Server is running on: http://localhost:${config.port}/`)
        console.info(`API docs: http://localhost:${config.port}/api-docs`)
        console.info(`GraphQL Server: http://localhost:${config.port}/graphql`)
    })
})
