# 1. Get an account on `GitLab`

Go to [Gitlab](https://gitlab.com/users/sign_up) and get an account. We recommend using your `github` when signing up.
![Gitlab-Sing-up](examples/gitlab_sign_up.png)

1.2 Add your credit card to validate you account(you will not be able to run pipelines if you do not do this).

See [why Gitlab asks for this her](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/).

1.3 Add your `.ssh` key to the `Gitlab` account. See the [step by step guide here](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account).

**The two steps above are critical for you to be able to push code and run pipelines on Gitlab!**

_If you already have a Gitlab account an the `ssh` key setup you can skip the two steps above._

---

# 2. Create an account on `AWS`

Go to [AWS](https://portal.aws.amazon.com/billing/signup#/start) and create an account.

---

# 3. Create a `GitLab` repository for the app

### 3.1 Login to `Gitlab` by clicking [here](https://gitlab.com/users/sign_in)

---

### 3.2 Go to [Projects](https://gitlab.com/dashboard/projects) and click on **New Project**:

![New Project](examples/new_project_gitlab.png)

---

### 3.3 In the next step select **Blank Project**:

![Blank Project](examples/blank_project_gitlab.png)

---

### 3.4 Add a name: **`ci-cd-rest-api`** for example and remove the _README_ option:

![Setup the Project](examples/new_project_gitlab_setup.png)

---

### 3.5 Click **Create Project**, you should see this:

![New Project](examples/new_project_view_gitlab.png)

Congratulations! In the next step, we will change the _origin_ of the current repository and host our code on _Gitlab_ instead.

---

### 3.6 Change the origin of our repository to the `Gitlab`

From the project page you have created above, scroll to the bottom and copy the code at the bottom of the page, leaving out the first line:

![New Project](examples/push_an_existing_folder.png)

Paste that in a terminal window, inside your repo:

```bash
git remote rename origin old-origin
git remote add origin git@gitlab.com:YOUR_USERNAME/YOUR_REPOSITORY
git push -u origin --all
git push -u origin --tags
```

You should see something like this:

![New Project](examples/push_success.png)

**Note: If you get errors here, make sure you review step #1**

Your code should be now visible in the `Repository` tab:

![Repo Setup](examples/repository_setup.png)

---

## You are to continue

You can now go back to [Readme.md](README.md) and continue with the setup from there.

---

### Made with :orange_heart: in Berlin by @theSeniorDev
